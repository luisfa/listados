---

layout: post

section: communities

title: EDUCATIC (txs.ES)

---

## Información

-   Temáticas principales: Promoción de tecnologías éticas en educación. Propugna la creación de una fundación con más amplios objetivos (promoción de tecnologías éticas para la sociedad). Uno de los criterios es que el software sea libre, pero añade la defensa de la protección de los datos personales (soberanía digital). Esta comunidad ofrece servicios de Jabber, Nextcloud, BigBlueButton, correo, Mastodon, y Peertube, entre otros.

## Contacto

-   Sede física: Deán Palahí, 16. La Laguna. Tenerife

-   Página web: <https://educatic.txs.es/>

-   Email: educatic@txs.es

-   Mastodon (u otras redes sociales libres): <https://txs.es/@educatic>

-   Twitter:

-   Sala en Matrix:

-   Grupo o canal en Telegram:

-   GitLab/GitHub (u otros sitios de código colaborativo):

